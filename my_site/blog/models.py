from django.db import models
from django.core.validators import MinLengthValidator


# Create your models here.

class Author(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField()

    def full_name(self):
        return f"{self.first_name} {self.last_name}"
    
    def __str__(self):
        return self.full_name()

class Tag(models.Model):
    caption = models.CharField(max_length=20)

    def __str__(self):
        return self.caption


class Post(models.Model):
    title = models.CharField(max_length=150)
    excerpt = models.CharField(max_length=250)
    image = models.ImageField(upload_to="posts", null=True)
    date = models.DateField(auto_now=True)
    # auto_now updates the date whenever data is modified
    slug = models.SlugField(unique=True)
    # index is automatically created for slugs, or when unique=True
    content = models.TextField(validators=[MinLengthValidator(10)])
    # RELATIONS
    author = models.ForeignKey(Author, on_delete=models.SET_NULL, null=True, related_name="posts")
    # if an author is deleted, author field in post is left w null
    tags = models.ManyToManyField(Tag)

    def __str__(self):
        return self.title

# Add a Comment Model that has a 1 to many relationship with Post (add foreign key)
    
class Comment(models.Model):
    user_name = models.CharField(max_length=120)
    user_email = models.EmailField()
    text = models.TextField(max_length=400)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="comments")
    # related name so that on a gicen post we can access all comments through a comments field 
    

