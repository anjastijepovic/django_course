# from typing import Any
# from django.db.models.query import QuerySet
from django.shortcuts import render, get_object_or_404
# from datetime import date
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import ListView, DetailView
from django.views import View

from .models import Post
from .forms import CommentForm


# dummy data
'''
all_posts = [
    {
        "slug": "hike-in-the-mountains",
        "image": "mountains.jpg",
        "author": "Maximilian",
        "date": date(2021, 7, 21),
        "title": "Mountain Hiking",
        "excerpt": "There's nothing like the views you get when hiking in the mountains! And I wasn't even prepared for what happened whilst I was enjoying the view!",
        "content": """
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis nobis
          aperiam est praesentium, quos iste consequuntur omnis exercitationem quam
          velit labore vero culpa ad mollitia? Quis architecto ipsam nemo. Odio.

          Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis nobis
          aperiam est praesentium, quos iste consequuntur omnis exercitationem quam
          velit labore vero culpa ad mollitia? Quis architecto ipsam nemo. Odio.

          Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis nobis
          aperiam est praesentium, quos iste consequuntur omnis exercitationem quam
          velit labore vero culpa ad mollitia? Quis architecto ipsam nemo. Odio.
        """
    },
]
'''


# helper funct
# def get_date(post):
#     return post['date']

# FUNCTION BASED VIEWS: 

'''
def starting_page(request):
    latest_posts = Post.objects.all().order_by("-date")[:3]
    # ordered in descending way, and get first 3. -3 wouldnt work in django

# for dummy data
    # we wanna get the 3 latest posts for the starting page
    # sorted_posts = all_posts.sort(key=get_date)
    # ovo bi sortiralo staru listu a mi ocemo da napravimo novu listu koja je sortirana pa onda koristimo sorted:
    # sorted_posts = sorted(all_posts, key=get_date)
    # # sort posts by date
    # latest_posts = sorted_posts[-3:]
    # we wanna pass this to the render funct
    return render(request, "blog/index.html", {
        "posts": latest_posts 
    })
'''

'''
def posts(request):
    all_posts = Post.objects.all().order_by("-date")
    return render(request, "blog/all-posts.html", {
        "all_posts": all_posts
    })
'''

'''
def post_detail(request, slug):
    identified_post = get_object_or_404(Post, slug=slug)
    # dummy data
    # ne zaboravi slug !!
    #use next funct that finds the next el that matches a certain condition, and the cond is a list comprehension
    # identified_post = next(post for post in all_posts if post['slug'] == slug)
    # go through all posts, look for the slug field in every dict, and check if the slug is equal to the slug that is the parameter of the function, return the post where it matches
    return render(request, "blog/post-detail.html", {
        "post": identified_post,
        "post_tags": identified_post.tags.all()
    })
'''    


# CLASS BASED VIEWS practice

# Starting Page Class View

# fetching a list of posts (only 3 of them) and rendering the index.html template
# Najbolja solucija da se pretvori to u class view je LIST VIEW
# template name je taj index html koji treba da se renderuje, tj njegov path
# model za koji data treba da bude fetched je Post
#  posts treba da budu ordered in a descending order na osnovu date, i samo 3 treba da budu fetched, pa treba da overwrite neke metode
# get query set controls how data should be queried. by default it fetches all posts but we want to limit that to 3
# ordering je lista fields po kojoj zelimo da order by, al mi zelimo samo po datumu, descending pa je minus ispred
# posto smo ad promijenili u class view, dodati to i u blog > urls.py
# context_object_name - za ListView je by default object_list, i pod tim imenom bi templates mogli da koriste that fetched data. 
# mi imamo for petlju for post in POSTS, u templates, pa zelimo da se ta fetched data zove posts i zato promijenimo ime

class StartingPageView(ListView):
    template_name = "blog/index.html"
    model = Post
    ordering = ["-date"]
    context_object_name = "posts"

    def get_queryset(self):
        queryset = super().get_queryset() #uzima sve posts
        data = queryset[:3]
        return data
    

# All Posts Class View
    
# fetching all posts in a date descending order and rendering the all-posts.html template
    
class AllPostsView(ListView):
    template_name = "blog/all-posts.html"
    model = Post
    ordering = ["-date"]
    context_object_name = "all_posts"


# Single Post Class View
    
# fetching single post data and rendering post-detail.html template pa je ovo Detail View
    # django will automatically serach by slug and raise a 404 error so we dont need to manually add that
    # tags are not showing up anymore, so we need to overwrite the get context data method
    # we wanna add a new field to the context, posts_tags (to ime koristimo u templates)
    # self.object je single post date that we fetched, on this object we can access any of the fields from the Post model
    # self.object.tags.all() accesses all the tags for one fetched post
    # return updated context

# nakon sto smo dodali formu za komentare na post-detail.html:
    # ocemo da dodamo da singlepostview handles post requests as well (when users submit a comment)
    # zato smo iz detailed view prebacili u obicni View, sa get i post metodama
    # tu moramo manually da define get metodu, zato sto sada imamo i post metodu
    
class SinglePostView(DetailView):
    # template_name = "blog/post-detail.html"
    # model = Post
    # ### to je bilo za DetailView

    # when we load this page, we wanna include info whether this post is saved in our session or not
    # bc we wanna show the Read Later button if it is not stored in a session, or Remove from Read later if it is

    def is_stored_post(self,request, post_id):
        stored_posts = request.session.get("stored_posts")
        if stored_posts is not None:
            is_saved_for_later = post_id in stored_posts
        else:
            is_saved_for_later = False
        return is_saved_for_later
    

    def get(self, request, slug):
        post = Post.objects.get(slug=slug)
        context = {
            "post": post,
            "post_tags": post.tags.all(),
            "comment_form": CommentForm(),
            "comments": post.comments.all().order_by("-id"),
            # higher ids (most recent comments) are first in the list
            "saved_for_later": self.is_stored_post(request, post.id)
        }
        return render(request, "blog/post-detail.html", context)

    def post(self, request, slug):
        # validate the submitted form and add that comment to the db or show the errors
        comment_form = CommentForm(request.POST)
        # request.POST has the submitted data
        post = Post.objects.get(slug=slug)

        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.post = post
            comment.save()
            # this adds the comm to the db because it is a part of a modelform so it has the save() method
            # prvo uzmemo submitted data, ali korisnik ne submittuje post polje jer to makosmo, pa smo to dodali sa comment.post
            return HttpResponseRedirect(reverse("post-detail-page", args=[slug]))
            # we wanna create a url which points at the posts-detail page again. we already are on that page but we wanna redirect to it so a get req is sent
           #because we wanna get the posts again as well as the newly added comment
            # args su slugs from the page we are coming from, a taj slug je part of the url where the post req is sent to (pogledaj urls i post-detail.html)
        else:
            context = {
                "post": post,
                "post_tags": post.tags.all(),
                "comment_form": comment_form,
                "comments": post.comments.all(),
                "comments": post.comments.all().order_by("-id"),
                "saved_for_later": self.is_stored_post(request, post.id)
            }
            return render(request, "blog/post-detail.html", context)


    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context["post_tags"] = self.object.tags.all()
    #     context["comment_form"] = CommentForm()
    #     return context
    # ### to je bilo za DetailView
 

#  Read Later Class View (za read later posts)
        
# add post id to stored posts and then redirect to the read later page with posts we want to read later
    # post method - to add a post to the stored posts list
    # get - to get the stored posts posts and display them to the read later page
        #ako ne postoje posts u stored posts onda set the keys in context to an empty list and False
        # ako ih ima, fetch them from the db:
            #we wanna get objects from the Post model, but not all of them, just the ones with the ids stored in stored posts - za to koristimo filter i in modifier
    # poslije svega se renderuje stored-posts.html template

class ReadLaterView(View):
    def get(self, request):
        stored_posts = request.session.get("stored_posts")

        context = {}

        if stored_posts is None or len(stored_posts) == 0:
            context["posts"] = []
            context["has_posts"] = False
        else:
          posts = Post.objects.filter(id__in=stored_posts)
          context["posts"] = posts
          context["has_posts"] = True

        return render(request, "blog/stored-posts.html", context)


    def post(self, request):
        stored_posts = request.session.get("stored_posts")

        if stored_posts is None:
          stored_posts = []

        post_id = int(request.POST["post_id"])

        if post_id not in stored_posts:
          stored_posts.append(post_id)
        else:
          stored_posts.remove(post_id)

        request.session["stored_posts"] = stored_posts
        
        return HttpResponseRedirect("/")