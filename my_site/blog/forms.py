from django import forms

from .models import Comment

# we want this form to appear on the post-detail.html page so people could leave comments on every single post

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        # model to which this form is related to
        exclude = ["post"]
        # fields that the user should see. the user shouldnt see the post field
        labels = {
            "user_name": "Your Name",
            "user_email": "Your Email",
            "text": "Your Comment"
        }
        # we can change the default labels. for ex, for user_name default label is User Name, and we want Your Name to be on the form
        # this post is passed to the post-detail.html template
        # add this form to the SinglePostView in views.py and then interpolate it in post-detail.html