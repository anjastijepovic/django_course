from django import forms
from .models import Review

# class ReviewForm(forms.Form):
#     user_name = forms.CharField(label="Your Name", max_length=100, error_messages={
#         "required": "Your name must not be empty", # label by default is User name
#         "max_length": "Please enter a shorter name"
#     })
#     review_text = forms.CharField(label="Your Feedback", widget=forms.Textarea, max_length=200)
#     rating = forms.IntegerField(label="Your rating", min_value=1, max_value=5)

# Another way to create forms: modelforms

class ReviewForm(forms.ModelForm):
    # nested meta class to tell Django to which model is this class related to
    class Meta:
        model = Review
        fields = "__all__"
        # mora eksplicitno da se kaze djangu koje fields iz modela da doda u formu. moze ili all, ili all pa doda se exclude = [ lista tih koji treba da se exclude ], ili fields = [ ... ]
        labels = {
            "user_name": "Your Name",
            "review_text": "Your Feedback",
            "rating": "Your Rating"
        }
        error_messages: {
            "user_name": {
                "required": "Your name must not be empty",
                "max_length": "Please enter a shorter name"
            }
        }