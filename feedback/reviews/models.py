from django.db import models

# Create your models here.

class Review(models.Model):
    user_name = models.CharField(max_length=100)
    # Ne moramo da validate max length jer to vec imamo u inputima, ali treba for the db setup
    review_text = models.TextField()
    rating = models.IntegerField()


    