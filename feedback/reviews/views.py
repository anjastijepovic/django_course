from typing import Any
from django.db.models.query import QuerySet
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.views import View
from django.views.generic.base import TemplateView
from django.views.generic import ListView, DetailView
from django.views.generic.edit import FormView, CreateView


from .forms import ReviewForm
from .models import Review


# Create your views here.

# Function based views

'''
def review(request):
    if request.method == 'POST':
        form = ReviewForm(request.POST)
        # .POST gives us access to the entered data. we have a 'username' input name in the form
        if form.is_valid():
            # is_valid je django built in
            # review = Review(user_name=form.cleaned_data['user_name'], review_text=form.cleaned_data['review_text'], rating=form.cleaned_data['rating'])
            # review.save()
            # cleaned in je isto built in

            # sa ModelForm je moguće savo da save the data:
            form.save()
            return HttpResponseRedirect("/thank-you")
            # redirect to the thank you page ako jeste valid
    else:
        # ako nije post req nego get, onda pravimo novu formu
        form = ReviewForm()
    return render(request, "reviews/review.html", {
        "form": form
    })

def thank_you(request):
    return render(request, "reviews/thank_you.html")
'''

# CLASS VIEWS

# views kao klase. def metode get i post pa nije potrebna ona dodatna if provjera da li je get ili post

# class ReviewView(View):
#     # mi extendujemo iz View ali imaju i druge Views we can inherit from, like ListView ili ClassView
#     def get(self, request):
#         form = ReviewForm()
            
#         return render(request, "reviews/review.html", {
#         "form": form
#         })

#     def post(self, request):
#         form = ReviewForm(request.POST)

#         if form.is_valid():
#             form.save()
#             return HttpResponseRedirect("/thank-you")
        
#         return render(request, "reviews/review.html", {
#         "form": form
#         })
# promijeni i u urls.py to point at ReviewView


# # Laksi nacin sa FormView
# class ReviewView(FormView):
#     # which form class should be used for rendering the form and validating the input data
#     form_class = ReviewForm
#     # which template should be used for it
#     template_name = "reviews/review.html"
#     # define the page where you wanna redirect to when the form is submitted
#     success_url =  "/thank-you"

#     # this method will be run by django when a valid form is submitted. to be saved in the db
#     def form_valid(self, form):
#         form.save()
#         return super().form_valid(form)

# Laksi nacin sa CreateView
class ReviewView(CreateView):
    model = Review
    # we dont need to specify the form class, it just needs the model to create data
    # if u use createview, u dont even need to make your own form in forms.py
    # But if u do wanna define the form class, because of the error messages and ev, you define it here
    form_class = ReviewForm
    template_name = "reviews/review.html"
    success_url =  "/thank-you"


class ThankYouView(TemplateView):
    template_name = "reviews/thank_you.html"
    #template_name je path templejta dje zelimo da budemo redirected

    # ako imamo neku dynamic data u thank you templ:
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["message"] = "This works!"
        return context


class ReviewListView(ListView):
    # on the review_list page, we wanna output all the reviews we got
    template_name = "reviews/review_list.html"
    model = Review
    context_object_name = "reviews"
    # this lets us specify the name of the data which will be exposed to our template jer je default object_list. to ime onda koristimo u for petlji u review_list.html 

    # ako ocwmo samo reviews iznad 4
    # def get_queryset(self):
    #     base_query = super().get_queryset()
    #     data = base_query.filter(rating__gt=4)
    #     return data


class SingleReviewView(DetailView):
    template_name = "reviews/single_review.html"
    model = Review

    # this will contain the single fetched review
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # add more data to that context, is_favorite key that has true or false values depending if the loaded review id is the same as the favorite id
        loaded_review = self.object
        request = self.request
        favorite_id = request.session.get("favorite_review")
        context["is_favorite"] = favorite_id == str(loaded_review.id)
        # str jer je review_id stored as a str
        return context


class AddFavoriteView(View):
    # get the review id and mark it as our favorite
    def post(self, request):
        review_id = request.POST['review_id']
        # fav_review =Review.objects.get(pk=review_id)
        # to get the full review object based on that id, ali to ne mozemo da store in a session jer je objekat, ocemo samo id da store
        request.session["favorite_review"] = review_id
        # fav_review gets stored in the session w the key favorite_review
        return HttpResponseRedirect("/reviews/" + review_id)
        # ovo moze i sa review funct
