from django.contrib import admin
from .models import Book, Author, Address, Country
# Register your models here.

class BookAdmin(admin.ModelAdmin):
    # readonly_fields = ("slug",)
    prepopulated_fields = {'slug': ("title",)}
    # for configuring Admin site

    # fields for filtering the table
    list_filter = ("author", "rating",)
    # what do you want to display on the admin page
    list_display = ("title", "author",)


admin.site.register(Book, BookAdmin)
admin.site.register(Author)
admin.site.register(Address)
admin.site.register(Country)


