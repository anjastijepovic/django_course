from django.urls import path
from . import views

urlpatterns = [
    path("", views.index),
    # path("<int:id>", views.book_detail, name="book-detail"),
    # book_detail html, isto se zove id u arg
    path("<slug:slug>", views.book_detail, name="book-detail"),

]