from django.shortcuts import render, get_object_or_404
from django.http import Http404
from .models import Book
from django.db.models import Avg, Max, Min

# Create your views here.

def index(request):
    # get all books
    books = Book.objects.all().order_by("-rating") #-rating je descending order
    num_books = books.count()
    avg_rating = books.aggregate(Avg("rating"))
    return render(request, "book_outlet/index.html", {
        "books": books,
        "total_number_of_books": num_books,
        "average_rating": avg_rating
    })

# def book_detail(request, id):
    # try:
    #     book = Book.objects.get(pk=id)
    # # primary key = id iz argumenta
    # except:
    #     raise Http404()
    # ili krace
    # book = get_object_or_404(Book, pk=id)

def book_detail(request, slug):
    book = get_object_or_404(Book, slug=slug)
    return render(request, "book_outlet/book_detail.html", {
        "title": book.title,
        "author": book.author,
        "rating": book.rating,
        "is_bestseller": book.is_bestselling
    })