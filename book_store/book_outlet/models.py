from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.urls import reverse
from django.utils.text import slugify


# Create your models here.

class Country(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=5)

    def __str__(self):
        return f"{self.name}"
    
    class Meta:
        verbose_name_plural = "Countries"

class Address(models.Model):
    street = models.CharField(max_length=100)
    postal_code = models.CharField(max_length=5)
    city = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.street}, {self.postal_code}, {self.city}"
    
    class Meta:
        # nested class with special settings in it to determine how django treats the Address class
        verbose_name_plural = "Address Entries"
        # instead of Addresses
    

class Author(models.Model):
    first_name = models.CharField(max_length=100)
    last = models.CharField(max_length=100)
    address = models.OneToOneField(Address, on_delete=models.CASCADE, null=True)
    # for 1 to 1 rel, we dont need the related_name field bc it is author by default

    def full_name(self):
        return f"{self.first_name} {self.last}"
    
    def __str__(self):
        return self.full_name()


class Book(models.Model):
    title = models.CharField(max_length=50)
    # charfield data type, small or medium text. max len arg is required
    rating = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)])
    #it automatically adds an id field, autoincrement
    # ako odje promijenimo nesto, moramo opet da run migrations
    author = models.ForeignKey(Author, on_delete=models.CASCADE, null=True, related_name="books")
    is_bestselling = models.BooleanField(default=False)
    # by default je false, ovo moramo da damo da bi radili drugu opciju za make migrations
    slug = models.SlugField(default="", blank=True, null=False, db_index=True) # so the url would look like this harry-potter-1
    # indexes for fields that are commonly used for searching data, better efficiency
    published_countries=models.ManyToManyField(Country, null=False, related_name="books")
    # kao lista drzava

    def get_absolute_url(self):
        # return reverse("book-detail", args=[self.id])
        return reverse("book-detail", args=[self.slug])
    
    # def save(self, *args, **kwargs):
    #     self.slug = slugify(self.title)
    #     super().save(*args, *kwargs)
    # overwriting save method so we could make the slug for url based on the title of the book
        
    def __str__(self):
        return f"{self.title} ({self.rating})"
    # when we get all entries with Book.objects.all()


