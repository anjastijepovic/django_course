from django.urls import path
from . import views
# from the same folder

# url config
urlpatterns = [
    path("", views.index, name="index"), #/challenges/
    # path("january", views.january),
    # if a request reaches january, execute index funct from views.py
    # http://127.0.0.1:8000/challenges/january
    # prvi dio url je u monthly_challenges/urls.py a drugi dio, january, je odje
    # path("february", views.february),

    # DYNAMIC PATH, Handle all urls with any text after challenges/
    path("<int:month>", views.monthly_challenge_by_number),
    path("<str:month>", views.monthly_challenge, name = "month-challenge") #/challenges/month
    # treba da bude str ili converted to a str(to ne moramo da dodamo)
    # ako je broj, execute prvu fju, ako je string, drugu
]