from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect, Http404
from django.urls import reverse
# from django.template.loader import render_to_string
# so we render the html file to string


monthly_challenges = {
    "january": "Eat healthy!",
    "february": "Walk for 20 mins or more every day.",
    "march": "Eat healthy!",
    "april": "Walk for 20 mins or more every day.",
    "may": "Eat healthy!",
    "june": "Walk for 20 mins or more every day.",
    "july": "Eat healthy!",
    "august": "Walk for 20 mins or more every day.",
    "september": "Eat healthy!",
    "october": "Walk for 20 mins or more every day.",
    "november": "Eat healthy!",
    "december": None
}


def index(request):
    # listing an index of all available months
    # makes a list of months we can click and it will redirect us to that page
    # list_items = ""
    months = list(monthly_challenges.keys())

    return render(request, "challenges/index.html", {
        "months": months
    })
    # for month in months:
    #     capitalized_month = month.capitalize()
    #     month_path = reverse("month-challenge", args=[month])
    #     list_items += f"<li><a href=\"{month_path}\">{capitalized_month}</a></li>"
    # #  after this for loop we'll have 'li li li ... li'
    # response_data = f"<ul>{list_items}</ul>"
    # return HttpResponse(response_data)


# ova fja executes kad se u urlu unese broj mjeseca a ne text mjeseca
def monthly_challenge_by_number(request, month):
    months = list(monthly_challenges.keys())
    if month > len(months):
        return HttpResponseNotFound("Invalid month")

    redirect_month = months[month-1]
    # jer je month koji smo dobili zapravo integer
    # return HttpResponseRedirect("/challenges/" + redirect_month)
    # this path can be constructed dynamically by using reverse funct, that takes in the name of the path and arguments added to the url

    redirect_path = reverse(
        "month-challenge", args=[redirect_month])  # /challenges/month
    # with reverse, we select the target url by name and let django construct the full path
    return HttpResponseRedirect(redirect_path)


def monthly_challenge(request, month):
    # challenge_text = None
    # if month == "january":
    #     challenge_text = "Eat healthy!"
    # elif month == "february":
    #     challenge_text = "Walk for 20 mins or more every day."
    # else:
    #     return HttpResponseNotFound("This month is not supported")
    try:
        challenge_text = monthly_challenges[month]
        # response_data = render_to_string("challenges/challenge.html")
        # return HttpResponse(response_data)
        # krace:
        return render(request, "challenges/challenge.html", {
            "text": challenge_text,
            # for dynamically inserting into html file
            "month_name": month
        })
    except:
        raise Http404()
